﻿using StorageServices.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace StorageServices.Contracts.Driver
{
    public interface IDriverServices
    {
        List<DriverInfoModel> GetAllDrivers();

        void AddDriverInfo(DriverInfoModel model);

        DriverInfoModel GetDriverById(int id);

        void SetStatusDriverById(int id, string status);

        string GetStatusDriver(int id);

        void SetTimeSecondsDriver(int id, int timeSeconds);
    }
}
